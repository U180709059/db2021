use movie_db;
use mydb;
# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
select title #we can write here ,budget,ranking
from movies
where budget > 10000000 and ranking < 6;

# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
select title
from movies join genres on movies.movie_id = genres.movie_id
where genre_name="Action"  and rating > 8.8 and  year>2009;

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
# we can also use join like above but we tried nested query  in this.Subquery is in the parantesis
select title
from movies
where duration>150 and oscars>2 and movie_id in (
select movie_id
from genres
where genre_name="Drama") ;

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
select title, oscars
from movies mv1 join (select movie_id, count(movie_id) as mv 
    from movie_stars ms join stars s on ms.star_id = s.star_id
    where star_name = "Orlando Bloom" or star_name="Ian McKellen"
    group by movie_id having mv > 1) as table1
on mv1.movie_id = table1.movie_id
where oscars > 2;

# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 
select title,votes,year
from movies m join(select  director_name,movie_id
	from movie_directors md join directors d on md.director_id=d.director_id
	where director_name="Quentin Tarantino") tb1 on m.movie_id=tb1.movie_id
where votes>500000 and year<2000;

# 6. Show the thriller films whose budget is greater than 25 million$.	 
select title, genre_name,budget
from movies join genres on movies.movie_id=genres.movie_id
where genre_name="thriller" and budget>25000000;

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
select table1.title,table1.genre_name,language_name,table1.year
from languages l join(select movies.movie_id,title, genre_name,year
	from movies join genres on movies.movie_id=genres.movie_id
	where genre_name="drama" and year>1990 and year<2000) table1 on l.movie_id=table1.movie_id
where language_name=" Italian";
 
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 
select title, table1.star_name,oscars
from movies mv1 join (select movie_id,star_name
    from movie_stars ms join stars s on ms.star_id = s.star_id
    where star_name = "Tom Hanks" ) as table1
on mv1.movie_id = table1.movie_id
where oscars > 3;

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select table2.title, table1.country_name, table2.genre_name, table2.duration
from
    (select movie_id, country_name
    from countries cs join producer_countries pcs on cs.country_id = pcs.country_id
    where country_name="USA") as table1
join
    (select mv.movie_id, title, genre_name, duration
    from movies mv join genres gn on mv.movie_id = gn.movie_id
    where genre_name="History" and duration>100 and duration<200) as table2
on table1.movie_id = table2.movie_id;

# 10.Compute the average budget of the films directed by Peter Jackson.
select title,avg(budget) avgBudget,table2.director_name
from movies m join  (select director_name,movie_id
	from directors d join movie_directors md on d.director_id=md.director_id
	where director_name="Peter Jackson") table2 on m.movie_id=table2.movie_id;

# 11.Show the Francis Ford Coppola film that has the minimum budget.
select title,min(budget) minBudget,table2.director_name
from movies m join  (select director_name,movie_id
	from directors d join movie_directors md on d.director_id=md.director_id
	where director_name="Francis Ford Coppola") table2 on m.movie_id=table2.movie_id;

# 12.Show the film that has the most vote and has been produced in USA.
select title,max(votes) maxVote,table1.country_name
from movies m join (select movie_id,country_name
	from producer_countries pd join countries c on pd.country_id=c.country_id
	where country_name="USA")as table1 on m.movie_id=table1.movie_id;


select customers.CustomerID,OrderID
from customers left join orders on customers.CustomerID=orders.CustomerID;

select customers.CustomerID,OrderID
from customers right join orders on customers.CustomerID=orders.CustomerID;

select  customers.CustomerID,OrderID
from customers join orders on customers.CustomerID=orders.CustomerID;