select count(*) from proteins;

explain select * from proteins where pid like "5HT2C_HUMA%";

create index idx1 on proteins(pid);
# column is contain only unique values

create unique index idx2 on proteins(accession);

#PK ve FK oluşturduğumuzda da aslında unique index oluşturuyoruz ama şu anki indexlerden farklı özellikte oluyor
#we can have multiple unique indexes in a single entity
#we cannot have more than one primary key constrains in an entity (like pk1,pk2)
#PK cannot return null value but index can

create unique index idx3 on proteins(accession,pid);#unique but composite index
#sıklıkla kullandığımız columnlara index koymak önemli

alter table proteins add constraint acc_pk primary key(accession);#implicit index
#we'll create a primary key constraint in out table and our pk will be accession

alter table proteins drop index idx3;#remove index


