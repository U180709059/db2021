use company;

update customers set Country = replace(Country, '\n', "");

select count(CustomerID) ,country
from customers
group by Country
order by count(CustomerID) desc;

create view usa_customers as
select CustomerName, ContactName
from customers
where Country = "USA";

SELECT * FROM company.usa_customers;

create or replace view products_above_avg as
select ProductName, Price
from products
where Price > (select avg(price) 
	from products);
    
SELECT * FROM company.products_above_avg;






